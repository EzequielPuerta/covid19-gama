# COVID19 GAMA Model

## Installation

#### 1. Clone:

`git clone https://gitlab.com/covid19-projects/covid19-microsimulation/covid19-gama-model`

#### 2. Enter to the project:

`cd covid19-gama-model`

#### 3. Set the right permissions to setup:

`chmod 777 setup.sh`

#### 4. Install:

`./setup.sh`

---

> Note: it will download the latest stable version of **GAMA Platform** and will prepare the *gama_platform* environment for correct *headless* usage

> ## [Back](/README.md)