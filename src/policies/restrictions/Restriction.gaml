/***
* Name: Restriction
* Author: Ezequiel Puerta
* Description: Restriccion abstracta
***/

model Restriction

import "../../species/agents/Individual.gaml"
import "NoRestrictions.gaml"
import "RadialDistance.gaml"
import "AllowExceptions.gaml"

species restriction schedules: [] {
	bool is_active <- true;
	bool restricting_to(individual person) {return false;} // when true, person cannot move
}