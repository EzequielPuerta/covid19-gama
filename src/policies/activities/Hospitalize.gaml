/***
* Name: Hospitalize
* Author: Ezequiel Puerta
* Description: Hospitalize
***/

model Hospitalize

import "Activity.gaml"


species hospitalize parent: immediate_activity {	
	init {
		min_start_hour <- 0 #hours;
		max_start_hour <- 24 #hours;
		granularity <- 10 #minutes;
		type <- treatment;
	}
	
	bool finished(scheduled_task current_task) {
		assert(self.type = current_task.task.type);
		return current_task.person.bio.is_recovered() or current_task.person.bio.is_out_of_treatment();
	}
	
	list<individual> concerning_population {
		return individual where 
			(!each.bio.is_on_treatment() and ((each.is_resting() or each.doing.task.for_quarantine()) and (each.bio.is_severe_symptomatic() or each.bio.is_icu_symptomatic())));
	}
	
	list<scheduled_task> generate_tasks_for(individual person) {
		list<scheduled_task> tasks <- [];
		hospital objective <- one_of(hospital where !each.is_full());
		if objective = nil {objective <- one_of(hospital);}
		tasks << create_uncertain_task(world.scheduler_singleton.current_week_day, world.scheduler_singleton.current_time, person, objective);
		return tasks;
	}
}