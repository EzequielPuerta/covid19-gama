/***
* Name: Activity
* Author: Ezequiel Puerta
* Description: Actividad abstracta
***/

model Activity

import "../Scheduler.gaml"
import "../../init_modules/SchedulerModule.gaml"
import "EssentialService.gaml"
import "GenericWork.gaml"
import "Hospitalize.gaml"
import "KidsAtPark.gaml"
import "KidsAtSchool.gaml"
import "SelfQuarantine.gaml"
import "SupplyingGoods.gaml"
import "WorkingOutside.gaml"

species activity schedules: [] {
	// Types
	string type;
	bool for_work {return type = working;}
	bool for_study {return type = studying;}
	bool for_quarantine {return type = quarantine;}
	bool for_treatment {return type = treatment;}
	bool for_out_of_map {return type = out_of_map;}
	bool for_supplying {return type = supplying;}
	bool for_playing_at_park {return type = playing_at_park;}
	
	// Attributes
	bool is_active <- true;
	int priority <- 0;
	float duration;
	bool is_recurrent;
	float min_start_hour;
	float max_start_hour;
	float granularity;
	
	// Auxs
	int time_interval {return (max_start_hour - min_start_hour) / granularity;}
	
	// Main
	action concerning_population of: individual virtual: true;
	action generate_tasks_for(individual person) of: scheduled_task virtual: true;
	action finished(scheduled_task current_task) of: bool virtual: true;
	
	scheduled_task create_certain_task(int selected_day, float the_start_time, float the_finish_time, individual the_person, abstract_target the_objective) {
		scheduled_task task <- create_uncertain_task(selected_day, the_start_time, the_person, the_objective);
		task.finish_time <- the_finish_time;
		if the_objective.is_concrete_target() {ask concrete_target(the_objective) {do register_as_recurrent(the_person);}}
		return task;}
			
	scheduled_task create_uncertain_task(int selected_day, float the_start_time, individual the_person, abstract_target the_objective) {
		scheduled_task result;
		create scheduled_task number: 1 with: [
			day::selected_day, 
			start_time::the_start_time,
			person::the_person, 
			objective::the_objective,
			task::self]
			{result <- self;}
		return result;}
}

species recurrent_activity parent: activity schedules: [] {
	init {is_recurrent <- true;}
	action concerning_population of: individual virtual: true;
	action generate_tasks_for(individual person) of: scheduled_task virtual: true;
	action finished(scheduled_task current_task) of: bool virtual: true;
	
	action apply_recurrently(scheduler_module the_scheduler_module) {
		loop person over: list<individual>(self.concerning_population()) {
			ask person {
				add myself to: self.activities;
				list<scheduled_task> generated_tasks <- myself.generate_tasks_for(self);
				loop new_task over: generated_tasks {
					ask the_scheduler_module {do register(new_task);}}}
		}
	}
}

species immediate_activity parent: activity schedules: [] {
	init {is_recurrent <- false;}
	action concerning_population of: individual virtual: true;
	action generate_tasks_for(individual person) of: scheduled_task virtual: true;
	action finished(scheduled_task current_task) of: bool virtual: true;
	
	action apply_immediately {
		if (world.scheduler_singleton.current_time >= min_start_hour) and (world.scheduler_singleton.current_time <= max_start_hour) {
			loop person over: list<individual>(self.concerning_population()) {
				ask person {
					add myself to: self.activities;
					list<scheduled_task> generated_tasks <- myself.generate_tasks_for(self);
					loop new_task over: generated_tasks {
						ask new_task {do execute();}}}
			}}
	}
}