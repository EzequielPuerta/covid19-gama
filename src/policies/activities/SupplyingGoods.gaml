/***
* Name: SupplyingGoods
* Author: Ezequiel Puerta
* Description: Comprar viveres
***/

model SupplyingGoods


import "Activity.gaml"

species supplying_goods parent: immediate_activity {
	float duration <- 1.5 #hours;
	
	init {
		type <- supplying;
		min_start_hour <- 9 #hours;
		max_start_hour <- 21 #hours;
		granularity <- 30 #minutes;
	}
	
	bool finished(scheduled_task current_task) {
		assert(self.type = current_task.task.type);
		return world.scheduler_singleton.current_time >= current_task.finish_time;
	}
	
	list<individual> concerning_population {
		return int(individual_amount*supplying_ratio) among shuffle(individual where ((each.bio.range.is_adult() or each.bio.range.is_older_adult()) and each.is_resting()));
	}
	
	list<scheduled_task> generate_tasks_for(individual person) {
		list<scheduled_task> tasks <- [];
		float start_time <- world.scheduler_singleton.current_time;
		float finish_time <- start_time + self.duration;
		concrete_target objective <- one_of(building where (each.type_activity = supplying_b));
		tasks << create_certain_task(world.scheduler_singleton.current_week_day, start_time, finish_time, person, objective);
		return tasks;
	}
}