/***
* Name: AbstractExperiment
* Author: Ezequiel Puerta
* Description: Experimento Abstracto
***/

model AbstractExperiment

import "../core/Loading.gaml"
import "../core/Functions.gaml"
import "../species/geography/Block.gaml"
import "../species/geography/CensusRadio.gaml"
import "../species/geography/Street.gaml"
import "../species/targets/Frontier.gaml"
import "../species/targets/Hospital.gaml"
import "../species/targets/Building.gaml"
import "../species/agents/Individual.gaml"
import "../species/vehicles/Vehicle.gaml"
import "../core/ExportCSV.gaml"

global {
	init {
		/* Loading on headless mode */			
		ask world {
			do load_xml_file;
			do build_demographic_data_from(extra_parameters_json_file_path);
			do build_synthetic_population;
			do build_possible_families;
			do configure_gis_parameters;
			do load_modules;
			do export_initial_csv;}}}
			
experiment abstract_experiment virtual: true type: gui benchmark: false {
	/* Experiment Parameters */
	parameter "Individual amount"			category: "Experiment Parameters"		var: individual_amount init: 5000;
	parameter "Display size"				category: "Experiment Parameters"		var: display_size;
	parameter "Individual speed (in mt/sec)"category: "Experiment Parameters"		var: individual_speed			unit: #km/#h;
	parameter "Starting date"				category: "Experiment Parameters"		var: starting_date;
	parameter "Time step (in seconds)"		category: "Experiment Parameters"		var: step						unit: #minutes;
	parameter "Steps between collect"		category: "Experiment Parameters"		var: steps_between_collect;
	parameter "Steps between dump"			category: "Experiment Parameters"		var: steps_between_dump;
	parameter "Seed"						category: "Experiment Parameters"		var: seed						init: 1.0;
	parameter "Execution mode"				category: "Experiment Parameters"		var: mode;
	parameter "Output path"					category: "Experiment Parameters"		var: output_path				init: '/mnt/Datos/GIT/covid19-gama-model/src/results/tmp';
	parameter "Headless file"				category: "Experiment Parameters"		var: xml_headless_file_path;
	
	/* Specific experiment parameters */
	parameter "Repeat"						category: "Specific Experiment Parameters"		var: repeat;
	parameter "Final Step"					category: "Specific Experiment Parameters"		var: final_step;
		
	/* Epidemiological parameters */
	parameter "Contact Distance (in meters)"				category: "Epidemiological Parameters"	var: contact_distance					unit: #meters;
	parameter "Patient zero amount"							category: "Epidemiological Parameters"	var: patient_zero_amount;
	parameter "Transmission Ratio"							category: "Epidemiological Parameters"	var: transmission_ratio					min: 0.08 		max: 0.30;
	parameter "Asymptomatic Ratio"							category: "Epidemiological Parameters"	var: asymptomatic_ratio					min: 0.30 		max: 0.95;
	parameter "Mild Ratio"									category: "Epidemiological Parameters"	var: mild_ratio							min: 0.0		max: 1.0;
	parameter "Severe Ratio"								category: "Epidemiological Parameters"	var: severe_ratio						min: 0.0		max: 1.0;
	parameter "ICU Ratio"									category: "Epidemiological Parameters"	var: icu_ratio							min: 0.0		max: 1.0;
	parameter "Mean latent days (in sec.)"					category: "Epidemiological Parameters"	var: mean_latent_days					unit: #days;
	parameter "St deviation latent days (in sec.)"			category: "Epidemiological Parameters"	var: st_deviation_latent_days			unit: #days;
	parameter "Mean incubation days (in sec.)"				category: "Epidemiological Parameters"	var: mean_incubation_days				unit: #days;
	parameter "St deviation incubation days (in sec.)"		category: "Epidemiological Parameters"	var: st_deviation_incubation_days		unit: #days;
	parameter "Mean mild recovery days (in sec.)"			category: "Epidemiological Parameters"	var: mean_mild_recovery_days			unit: #days;
	parameter "St deviation mild recovery days (in sec.)"	category: "Epidemiological Parameters"	var: st_deviation_mild_recovery_days	unit: #days;
	parameter "Mean severe recovery days (in sec.)"			category: "Epidemiological Parameters"	var: mean_severe_recovery_days			unit: #days;
	parameter "St deviation severe recovery days (in sec.)"	category: "Epidemiological Parameters"	var: st_deviation_severe_recovery_days	unit: #days;
	parameter "Mean icu recovery days (in sec.)"			category: "Epidemiological Parameters"	var: mean_icu_recovery_days				unit: #days;
	parameter "St deviation icu recovery days (in sec.)"	category: "Epidemiological Parameters"	var: st_deviation_icu_recovery_days		unit: #days;
	parameter "Days on surfaces (in sec.)"					category: "Epidemiological Parameters"	var: days_on_surfaces					unit: #days;

	/* Restrictions parameter */
	parameter "External workers amount"			category: "Restrictions Parameters" 	var: external_workers_amount;
	parameter "Essential workers amount"		category: "Restrictions Parameters" 	var: essential_workers_amount;
	parameter "Supplying ratio"					category: "Restrictions Parameters" 	var: supplying_ratio					min: 0.003		max: 0.007;
	parameter "Restrictions compliance ratio"	category: "Restrictions Parameters" 	var: restrictions_compliance_ratio		min: 0.30 		max: 0.95;
	parameter "People Get Self Quarantine"		category: "Restrictions Parameters" 	var: people_get_self_quarantine;
	parameter "Self quarantine compliance ratio"category: "Restrictions Parameters"		var: self_quarantine_compliance_ratio	min: 0.30 		max: 0.95;
	parameter "Allow Excepted"					category: "Restrictions Parameters" 	var: allow_excepted;
	parameter "Schools Are Open" 				category: "Restrictions Parameters" 	var: schools_are_open;
	parameter "Travel Radius (in meters)"		category: "Restrictions Parameters"		var: travel_radius						unit: #km;
	
	/* GIS parameters */
	parameter "Coordinate system input" 		category: "GIS Parameters" 	var: coordinate_system_input init: "EPSG:3857";
	parameter "Coordinate system output" 		category: "GIS Parameters" 	var: coordinate_system_output init: "EPSG:4326";
		parameter "Satellital image" 				category: "GIS Parameters" 	var: satellital_image_file_path
		on_change: {satellital_image <- image_file(satellital_image_file_path);} init: "/mnt/Datos/GIT/marcos-paz-department/satellital/satellital.tif";
	parameter "Streets shapefile" 				category: "GIS Parameters" 	var: streets_shapefile_file_path
		on_change: {streets_shapefile <- shape_file(streets_shapefile_file_path);} init: "/mnt/Datos/GIT/marcos-paz-department/streets/streets.shp";
	parameter "Routes shapefile" 				category: "GIS Parameters" 	var: routes_shapefile_file_path
		on_change: {routes_shapefile <- shape_file(routes_shapefile_file_path);} init: "/mnt/Datos/GIT/marcos-paz-department/routes/routes.shp";
	parameter "Houses shapefile" 				category: "GIS Parameters" 	var: houses_shapefile_file_path
		on_change: {houses_shapefile <- shape_file(houses_shapefile_file_path);} init: "/mnt/Datos/GIT/marcos-paz-department/houses/houses.shp";
	parameter "Blocks shapefile" 				category: "GIS Parameters" 	var: blocks_shapefile_file_path
		on_change: {blocks_shapefile <- shape_file(blocks_shapefile_file_path);} init: "/mnt/Datos/GIT/marcos-paz-department/blocks/blocks.shp";
	parameter "Workplaces shapefile"			category: "GIS Parameters" 	var: workplaces_shapefile_file_path
		on_change: {workplaces_shapefile <- shape_file(workplaces_shapefile_file_path);} init: "/mnt/Datos/GIT/marcos-paz-department/workplaces/workplaces.shp";
	parameter "Hospitals shapefile" 			category: "GIS Parameters" 	var: hospitals_shapefile_file_path
		on_change: {hospitals_shapefile <- shape_file(hospitals_shapefile_file_path);} init: "/mnt/Datos/GIT/marcos-paz-department/hospitals/hospitals.shp";
	parameter "Train stations shapefile" 		category: "GIS Parameters" 	var: train_stations_shapefile_file_path
		on_change: {train_stations_shapefile <- shape_file(train_stations_shapefile_file_path);} init: "/mnt/Datos/GIT/marcos-paz-department/train_stations/train_stations.shp";
	parameter "Frontiers shapefile" 			category: "GIS Parameters" 	var: frontiers_shapefile_file_path
		on_change: {frontiers_shapefile <- shape_file(frontiers_shapefile_file_path);} init: "/mnt/Datos/GIT/marcos-paz-department/frontiers/frontiers.shp";
	parameter "Census radios shapefile" 		category: "GIS Parameters" 	var: census_radios_shapefile_file_path
		on_change: {census_radios_shapefile <- shape_file(census_radios_shapefile_file_path);} init: "/mnt/Datos/GIT/marcos-paz-department/census_radio/census_radio.shp";
	
	/* JSON parameters */	
	parameter "JSON Extra Parameters File Path"	category: "JSON Parameters"	var: extra_parameters_json_file_path init: '/mnt/Datos/GIT/covid19-gama-model/src/use_cases/MarcosPazMunicipio/includes/extra_parameters.json';
	
	/* Generic layout */
	output {
		layout tabs: true parameters: true navigator: false editors: false consoles: true;}
}