/***
* Name: ParameterExploration
* Author: Ezequiel Puerta
* Description: Experimento para la exploracion de parametros
***/

model ParameterExploration

import "../experiments/AbstractExperiment.gaml"

experiment ParameterExploration type: batch parent: abstract_experiment repeat: repeat keep_seed: true until: cycle > final_step {
	parameter "Real symptomatic amount"			category: "Optimization Parameters"	var: real_symptomatic_amount			init: 0;
	parameter "Real dead amount"				category: "Optimization Parameters"	var: real_dead_amount					init: 0;
	
	parameter "Transmission Ratio"				category: "Parameters to Optimize"	var: transmission_ratio					min: 0.12	max: 0.15	step: 0.01;
	parameter "Asymptomatic Ratio"				category: "Parameters to Optimize"	var: asymptomatic_ratio					min: 0.17	max: 0.17	step: 0.10;
	parameter "Restrictions compliance ratio"	category: "Parameters to Optimize" 	var: restrictions_compliance_ratio		min: 0.50	max: 0.50	step: 0.10;
	parameter "Self quarantine compliance ratio"category: "Parameters to Optimize"	var: self_quarantine_compliance_ratio	min: 0.60	max: 0.60	step: 0.10;
			
	reflex export_parameter_exploration when: world.is_time_to_collect() {
    	loop sim over: ParameterExploration_model {
	    	ask world {
	    		float last_fit <-
		    		float(abs(
			    		((individual count each.bio.is_symptomatic()) - real_symptomatic_amount) + 
			    		((individual count each.bio.is_dead()) - real_dead_amount)));
    		
	    		do save_as_csv([
		    		'current_date'::string(current_date),
		    		'current_cycle'::cycle,
		    		'transmission_ratio'::transmission_ratio,
		    		'asymptomatic_ratio'::asymptomatic_ratio,
		    		'supplying_ratio'::supplying_ratio,
		    		'restrictions_compliance_ratio'::restrictions_compliance_ratio,
		    		'self_quarantine_compliance_ratio'::self_quarantine_compliance_ratio,
		    		'last_fit'::last_fit], sim.name, 'parameter_exploration.csv');}}}
		    		
	method exhaustive 
		minimize: abs(
    		((individual count each.bio.is_symptomatic()) - real_symptomatic_amount) + 
    		((individual count each.bio.is_dead()) - real_dead_amount));
}