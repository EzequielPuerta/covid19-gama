/***
* Name: BuildingsViralLoad
* Author: Ezequiel Puerta
* Description: Experimento con enfasis en la carga viral de los edificios
***/

model BuildingsViralLoad

import "../experiments/AbstractExperiment.gaml"

experiment BuildingsViralLoad parent: abstract_experiment type: gui autorun: false {		
	/* Collect data as CSV format */
	reflex collect_csv when: world.is_time_to_collect() {
		loop sim over: ExportCSV_model {
			ask sim.concrete_target {
				ask world {do collect_csv([
					'id'::myself.name,
					'current_viral_load_percentage'::myself.current_viral_load_percentage,
					'geometry'::world.as_wkt_polygon(myself)], sim.name, "buildings_viral_load.csv");}}}}
}