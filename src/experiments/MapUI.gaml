/***
* Name: MapUI
* Author: Ezequiel Puerta
* Description: Experimento para observar el mapa directamente desde la UI de Gama
***/

model MapUI

import "../experiments/AbstractExperiment.gaml"

experiment MapUI parent: abstract_experiment type: gui autorun: false benchmark: false {	
    output {
	    display map autosave: autosave_size type: java2D background: #black refresh: every(1 #cycles) ambient_light: 0 {
			light 1 type:direction direction:{cos(cycle*0.25/steps_on_a_minute+180),sin(cycle*0.25/steps_on_a_minute+180),-1} color: rgb(0,0,112*(1+cos(cycle*0.25/steps_on_a_minute+175)));
			light 2 type:direction direction:{cos(cycle*0.25/steps_on_a_minute),sin(cycle*0.25/steps_on_a_minute),-1} color: rgb(0,0,112*(1+cos(cycle*0.25/steps_on_a_minute+175)));
			light 3 type:direction direction:{0,0,-1} color:rgb(255*(1+cos(cycle*0.25/steps_on_a_minute+175)),255*(1+cos(cycle*0.25/steps_on_a_minute+175)),255*(1+cos(cycle*0.25/steps_on_a_minute+175)));
			
	    	image file: satellital_image refresh: false;
	    	species census_radio aspect: geom_density refresh: false transparency: 0.6;
	        species street aspect: geom refresh: false;
	        species individual aspect: geom refresh: true;        
	        species vehicle aspect: geom refresh: true;
	        species frontier aspect: geom refresh: false;
	        species hospital aspect: geom refresh: false;		
	    }   	
	}	
}