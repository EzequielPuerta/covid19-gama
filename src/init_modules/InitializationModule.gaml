/***
* Name: InitializationModule
* Author: Ezequiel Puerta
* Description: Modulo Abstracto GIS
***/

model InitializationModule

import "BlocksModule.gaml"
import "CensusRadiosModule.gaml"
import "FrontiersModule.gaml"
import "HospitalsModule.gaml"
import "HousesModule.gaml"
import "RoutesModule.gaml"
import "StreetsModule.gaml"
import "TrainStationsModule.gaml"
import "SchedulerModule.gaml"
import "WorkplacesModule.gaml"

species init_module schedules: [] {
	list<string> dependencies <- [];
	action initialize {}
}

species gis_module parent: init_module schedules: [] {
	shape_file shapefile;
	
	/* Identifiers */
	string blocksModuleId <- 'BlocksModule';
	string censusRadiosModuleId <- 'CensusRadiosModule';
	string frontiersModuleId <- 'FrontiersModule';
	string governmentModuleId <- 'GovernmentModule';
	string hospitalsModuleId <- 'HospitalsModule';
	string housesModuleId <- 'HousesModule';
	string routesModuleId <- 'RoutesModule';
	string schoolsModuleId <- 'SchoolsModule';
	string streetsModuleId <- 'StreetsModule';
	string trainStationsModuleId <- 'TrainStationsModule';
	string universitiesModuleId <- 'UniversitiesModule';	
}