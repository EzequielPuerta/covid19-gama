/***
* Name: StreetsModule
* Author: Ezequiel Puerta
* Description: Inicializador de calles segun capas GIS
***/

model StreetsModule

import "InitializationModule.gaml"
import "../core/Constants.gaml"
import "../species/geography/Street.gaml"

species streets_module parent: gis_module schedules: [] {
	action initialize {		
		world.shape <- envelope(shapefile);
		create street from: shapefile;
		street_network <- as_edge_graph(street);
	} 
}