/***
* Name: RoutesModule
* Author: Ezequiel Puerta
* Description: Inicializador de rutas para flujo de vehiculos externos segun capas GIS
***/

model RoutesModule

import "InitializationModule.gaml"
import "../core/Constants.gaml"
import "../species/geography/Route.gaml"

species routes_module parent: gis_module schedules: [] {
	action initialize {
		create route from: shapefile;
		routes_network <- as_edge_graph(route);
	} 
}