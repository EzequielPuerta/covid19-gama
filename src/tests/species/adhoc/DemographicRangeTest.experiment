/***
* Name: DemographicRangeTest
* Author: Ezequiel Puerta
* Description: Tests para modelo Rango Demografico (modelado como DemographicRange)
***/

model DemographicRangeTest

import "../../../species/adhoc/DemographicRange.gaml"

global {
	demographic_range young_range;
}

experiment DemographicRangeTest type: test autorun: true {
	setup {
		create demographic_range {
			self.min_age						<- 10;
			self.max_age 						<- 20;
			self.consider_as					<- young;
			self.total_percentage				<- 0.20;
			self.man_percentage					<- 0.45;
			self.woman_percentage				<- 0.55;
			self.man_fatality_rate				<- 0.002;
			self.woman_fatality_rate			<- 0.001;
			self.st_dev_for_man_fatality_rate	<- 0.0005;
			self.st_dev_for_woman_fatality_rate	<- 0.0004;
			young_range <- self;}
	}
	
	test "Attributes" {
		assert young_range.min_age 							= 10;
		assert young_range.max_age 							= 20;
		assert young_range.consider_as 						= young;
		assert young_range.total_percentage 				= 0.20;
		assert young_range.man_percentage 					= 0.45;
		assert young_range.woman_percentage 				= 0.55;
		assert young_range.man_fatality_rate 				= 0.002;
		assert young_range.woman_fatality_rate 				= 0.001;
		assert young_range.st_dev_for_man_fatality_rate 	= 0.0005;
		assert young_range.st_dev_for_woman_fatality_rate 	= 0.0004;
	}
	
	test "Testing" {
		assert young_range.is_young();
		assert !young_range.is_adult();
		assert !young_range.is_older_adult();
	}
	
	test "Generate age" {
		int iterations <- 1000;
		list<int> generated_ages;
		
		ask young_range {
			loop index over: range(1,iterations) {
				add generate_age() to: generated_ages;
			}
		}
		
		assert length(generated_ages where (each >= young_range.min_age)) = iterations;
		assert length(generated_ages where (each <= young_range.max_age)) = iterations;
	}
	
	test "Generate sex" {
		int men <- 0;
		int women <- 0;
		int iterations <- 10000;
			
		ask young_range {
			loop index over: range(iterations) {
				if generate_sex() = male {men <- men+1;} else {women <- women+1;}
			}
		}

		assert (abs((men/iterations) - young_range.man_percentage)) with_precision 1 = 0;
		assert (abs((women/iterations) - young_range.woman_percentage)) with_precision 1 = 0;
	}
}
