/***
* Name: Loading
* Author: Ezequiel Puerta
* Description: Loading
***/

model Loading

import "Constants.gaml"

global {		
	action load_xml_file {
		// ITS USED TO LOAD PARAMETERS SETTED IN XML HEADLESS FILE, NOT AS GAMA PARAMETERS, BUT AS EXPERIMENT, ENVIRONMENT OR SCENARIO PARAMETERS...
		create XMLFile {
			self.raw_file <- last(xml_file(world.xml_headless_file_path).contents);
			world.xml_headless_file <- self.parse();}
		
		// GENERAL (FOR ALL PURPOSES)
		if world.xml_headless_file.tag_named('snapshot_file_path').exists_at(['Simulation', 'Experiment']) {
			world.snapshot_file_path <- world.xml_headless_file.tag_named('snapshot_file_path').at(['Simulation', 'Experiment']);}
		if world.xml_headless_file.tag_named('final_step').exists_at(['Simulation', 'Experiment']) {
			world.final_step <- int(world.xml_headless_file.tag_named('final_step').at(['Simulation', 'Experiment']));}
		
		// JUST FOR OPTIMIZATION AND EXPLORATION EXPERIMENTS
		if world.xml_headless_file.tag_named('repeat').exists_at(['Simulation', 'Experiment']) {
			world.repeat <- int(world.xml_headless_file.tag_named('repeat').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('iter_max').exists_at(['Simulation', 'Experiment']) {
			world.iter_max <- int(world.xml_headless_file.tag_named('iter_max').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('real_symptomatic_amount').exists_at(['Simulation', 'Experiment']) {
			world.real_symptomatic_amount <- int(world.xml_headless_file.tag_named('real_symptomatic_amount').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('real_dead_amount').exists_at(['Simulation', 'Experiment']) {
			world.real_dead_amount <- int(world.xml_headless_file.tag_named('real_dead_amount').at(['Simulation', 'Experiment']));}
		
		// the idea was use this parameters in the optimization and exploration experiments to set parameter's intervals... but min values must be constants in GAMA :/
		if world.xml_headless_file.tag_named('min_transmission_ratio').exists_at(['Simulation', 'Experiment']) {
			world.min_transmission_ratio <- float(world.xml_headless_file.tag_named('min_transmission_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('max_transmission_ratio').exists_at(['Simulation', 'Experiment']) {
			world.max_transmission_ratio <- float(world.xml_headless_file.tag_named('max_transmission_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('step_transmission_ratio').exists_at(['Simulation', 'Experiment']) {
			world.step_transmission_ratio <- float(world.xml_headless_file.tag_named('step_transmission_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('min_asymptomatic_ratio').exists_at(['Simulation', 'Experiment']) {
			world.min_asymptomatic_ratio <- float(world.xml_headless_file.tag_named('min_asymptomatic_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('max_asymptomatic_ratio').exists_at(['Simulation', 'Experiment']) {
			world.max_asymptomatic_ratio <- float(world.xml_headless_file.tag_named('max_asymptomatic_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('step_asymptomatic_ratio').exists_at(['Simulation', 'Experiment']) {
			world.step_asymptomatic_ratio <- float(world.xml_headless_file.tag_named('step_asymptomatic_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('min_restrictions_compliance_ratio').exists_at(['Simulation', 'Experiment']) {
			world.min_restrictions_compliance_ratio <- float(world.xml_headless_file.tag_named('min_restrictions_compliance_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('max_restrictions_compliance_ratio').exists_at(['Simulation', 'Experiment']) {
			world.max_restrictions_compliance_ratio <- float(world.xml_headless_file.tag_named('max_restrictions_compliance_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('step_restrictions_compliance_ratio').exists_at(['Simulation', 'Experiment']) {
			world.step_restrictions_compliance_ratio <- float(world.xml_headless_file.tag_named('step_restrictions_compliance_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('min_self_quarantine_compliance_ratio').exists_at(['Simulation', 'Experiment']) {
			world.min_self_quarantine_compliance_ratio <- float(world.xml_headless_file.tag_named('min_self_quarantine_compliance_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('max_self_quarantine_compliance_ratio').exists_at(['Simulation', 'Experiment']) {
			world.max_self_quarantine_compliance_ratio <- float(world.xml_headless_file.tag_named('max_self_quarantine_compliance_ratio').at(['Simulation', 'Experiment']));}
		if world.xml_headless_file.tag_named('step_self_quarantine_compliance_ratio').exists_at(['Simulation', 'Experiment']) {
			world.step_self_quarantine_compliance_ratio <- float(world.xml_headless_file.tag_named('step_self_quarantine_compliance_ratio').at(['Simulation', 'Experiment']));}}
	
	action build_synthetic_population {
		world.synthetic_population <- [];
		loop gender over: [male, female] {
			loop age_stage over: [young, adult, older_adult] {
				world.synthetic_population << (gender::age_stage)::[];}}}
	
	action build_possible_families {
		world.possible_families <- [];
		
		// possible_families <- [family_size::family_shapes]
		// family_shapes <- [probability::family_type]
		// family_type <- [individual_type::amount]
		// individual_type <- sex::consider_as
		
		map<map<pair<string, string>, int>, float> families_of_1 <- [];
		families_of_1 << [(male::adult)::1]::0.25;
		families_of_1 << [(male::older_adult)::1]::0.25;
		families_of_1 << [(female::adult)::1]::0.25;
		families_of_1 << [(female::older_adult)::1]::0.25;
		
		map<map<pair<string, string>, int>, float> families_of_2 <- [];
		families_of_2 << [(male::adult)::1, (female::adult)::1]::0.60;
		families_of_2 << [(male::older_adult)::1, (female::older_adult)::1]::0.30;
		families_of_2 << [(male::adult)::1, (male::adult)::1]::0.05;
		families_of_2 << [(female::adult)::1, (female::adult)::1]::0.05;
		
		map<map<pair<string, string>, int>, float> families_of_3 <- [];
		families_of_3 << [(male::adult)::1, (female::adult)::1, (male::young)::1]::0.44;
		families_of_3 << [(male::adult)::1, (female::adult)::1, (female::young)::1]::0.44;
		families_of_3 << [(male::adult)::1, (male::adult)::1, (male::young)::1]::0.03;
		families_of_3 << [(male::adult)::1, (male::adult)::1, (female::young)::1]::0.03;
		families_of_3 << [(female::adult)::1, (female::adult)::1, (male::young)::1]::0.03;
		families_of_3 << [(female::adult)::1, (female::adult)::1, (female::young)::1]::0.03;
		
		map<map<pair<string, string>, int>, float> families_of_4 <- [];
		families_of_4 << [(male::adult)::1, (female::adult)::1, (male::young)::1, (female::young)::1]::0.28;
		families_of_4 << [(male::adult)::1, (female::adult)::1, (male::young)::2]::0.30;
		families_of_4 << [(male::adult)::1, (female::adult)::1, (female::young)::2]::0.30;
		families_of_4 << [(male::adult)::1, (female::adult)::1, (male::young)::1, (male::older_adult)::1]::0.03;
		families_of_4 << [(male::adult)::1, (female::adult)::1, (male::young)::1, (female::older_adult)::1]::0.03;
		families_of_4 << [(male::adult)::1, (female::adult)::1, (female::young)::1, (male::older_adult)::1]::0.03;
		families_of_4 << [(male::adult)::1, (female::adult)::1, (female::young)::1, (female::older_adult)::1]::0.03;
		
		map<map<pair<string, string>, int>, float> families_of_5 <- [];
		families_of_5 << [(male::adult)::1, (female::adult)::1, (male::young)::3]::0.20;
		families_of_5 << [(male::adult)::1, (female::adult)::1, (female::young)::3]::0.20;
		families_of_5 << [(male::adult)::1, (female::adult)::1, (male::young)::2, (female::young)::1]::0.20;
		families_of_5 << [(male::adult)::1, (female::adult)::1, (male::young)::1, (female::young)::2]::0.20;
		families_of_5 << [(male::adult)::2, (female::adult)::1, (male::young)::1, (female::young)::1]::0.02;
		families_of_5 << [(male::adult)::2, (female::adult)::1, (male::young)::2]::0.02;
		families_of_5 << [(male::adult)::2, (female::adult)::1, (female::young)::2]::0.02;
		families_of_5 << [(male::adult)::1, (female::adult)::2, (male::young)::1, (female::young)::1]::0.02;
		families_of_5 << [(male::adult)::1, (female::adult)::2, (male::young)::2]::0.02;
		families_of_5 << [(male::adult)::1, (female::adult)::2, (female::young)::2]::0.02;
		families_of_5 << [(male::adult)::1, (female::adult)::1, (male::young)::1, (male::older_adult)::1, (female::older_adult)::1]::0.02;
		families_of_5 << [(male::adult)::1, (female::adult)::1, (female::young)::1, (male::older_adult)::1, (female::older_adult)::1]::0.02;
		families_of_5 << [(male::adult)::1, (female::adult)::1, (male::young)::2, (male::older_adult)::1]::0.01;
		families_of_5 << [(male::adult)::1, (female::adult)::1, (female::young)::2, (male::older_adult)::1]::0.01;
		families_of_5 << [(male::adult)::1, (female::adult)::1, (male::young)::2, (female::older_adult)::1]::0.01;
		families_of_5 << [(male::adult)::1, (female::adult)::1, (female::young)::2, (female::older_adult)::1]::0.01;
		
		map<map<pair<string, string>, int>, float> families_of_6 <- [];
		families_of_6 << [(male::adult)::1, (female::adult)::1, (male::young)::4]::0.10;
		families_of_6 << [(male::adult)::1, (female::adult)::1, (female::young)::4]::0.10;
		families_of_6 << [(male::adult)::1, (female::adult)::1, (male::young)::3, (female::young)::1]::0.15;
		families_of_6 << [(male::adult)::1, (female::adult)::1, (male::young)::2, (female::young)::2]::0.20;
		families_of_6 << [(male::adult)::1, (female::adult)::1, (male::young)::1, (female::young)::3]::0.15;
		families_of_6 << [(male::adult)::2, (female::adult)::1, (male::young)::2, (female::young)::1]::0.03;
		families_of_6 << [(male::adult)::2, (female::adult)::1, (male::young)::1, (female::young)::2]::0.03;
		families_of_6 << [(male::adult)::2, (female::adult)::1, (male::young)::3]::0.03;
		families_of_6 << [(male::adult)::2, (female::adult)::1, (female::young)::3]::0.03;
		families_of_6 << [(male::adult)::1, (female::adult)::2, (male::young)::2, (female::young)::1]::0.03;
		families_of_6 << [(male::adult)::1, (female::adult)::2, (male::young)::1, (female::young)::2]::0.03;
		families_of_6 << [(male::adult)::1, (female::adult)::2, (male::young)::3]::0.03;
		families_of_6 << [(male::adult)::1, (female::adult)::2, (female::young)::3]::0.03;
		families_of_6 << [(male::adult)::1, (female::adult)::1, (male::young)::2, (male::older_adult)::1, (female::older_adult)::1]::0.02;
		families_of_6 << [(male::adult)::1, (female::adult)::1, (female::young)::2, (male::older_adult)::1, (female::older_adult)::1]::0.02;
		families_of_6 << [(male::adult)::1, (female::adult)::1, (male::young)::1, (female::young)::1, (male::older_adult)::1, (female::older_adult)::1]::0.02;
		
		world.possible_families <- [
			1::families_of_1,
			2::families_of_2,
			3::families_of_3,
			4::families_of_4,
			5::families_of_5,	
			6::families_of_6];}
	
	action build_demographic_data_from(string extra_parameters_path) {
		world.demographic_data <- [];
		map<string, unknown> settings <- (json_file(extra_parameters_path)).contents;
		
		loop population_range over: list(settings["demography"]) {
			create demographic_range {
				assert population_range["min_age"] != nil;
				assert population_range["max_age"] != nil;
				assert population_range["consider_as"] != nil;
				assert population_range["total_percentage"] != nil;
				assert population_range["man_percentage"] != nil;
				assert population_range["woman_percentage"] != nil;
				assert population_range["man_fatality_rate"] != nil;
				assert population_range["woman_fatality_rate"] != nil;
				assert population_range["st_dev_for_man_fatality_rate"] != nil;
				assert population_range["st_dev_for_woman_fatality_rate"] != nil;
				
				self.min_age						<- int(population_range["min_age"]);
				self.max_age 						<- int(population_range["max_age"]);
				self.consider_as					<- string(population_range["consider_as"]);
				self.total_percentage				<- float(population_range["total_percentage"]);
				self.man_percentage					<- float(population_range["man_percentage"]);
				self.woman_percentage				<- float(population_range["woman_percentage"]);
				self.man_fatality_rate				<- float(population_range["man_fatality_rate"]);
				self.woman_fatality_rate			<- float(population_range["woman_fatality_rate"]);
				self.st_dev_for_man_fatality_rate	<- float(population_range["st_dev_for_man_fatality_rate"]);
				self.st_dev_for_woman_fatality_rate	<- float(population_range["st_dev_for_woman_fatality_rate"]);
				add self to: world.demographic_data;}}
		assert length(world.demographic_data) > 0;
	}
	
	action configure_gis_parameters {		
		/* GIS Parameters : Mandatory files */
		assert coordinate_system_input 	!= nil;
		assert coordinate_system_output	!= nil;
		gama.pref_gis_default_crs		<- int(last(coordinate_system_input split_with ":"));
		gama.pref_gis_initial_crs		<- int(last(coordinate_system_input split_with ":"));
		gama.pref_gis_output_crs		<- int(last(coordinate_system_input split_with ":"));
		
		satellital_image				<- image_file(satellital_image_file_path);
		streets_shapefile 				<- shape_file(streets_shapefile_file_path,			coordinate_system_input);
		routes_shapefile 				<- shape_file(routes_shapefile_file_path,			coordinate_system_input);
		houses_shapefile 				<- shape_file(houses_shapefile_file_path,			coordinate_system_input);
		workplaces_shapefile			<- shape_file(workplaces_shapefile_file_path,		coordinate_system_input);
		hospitals_shapefile 			<- shape_file(hospitals_shapefile_file_path,		coordinate_system_input);
		frontiers_shapefile 			<- shape_file(frontiers_shapefile_file_path,		coordinate_system_input);
		census_radios_shapefile 		<- shape_file(census_radios_shapefile_file_path,	coordinate_system_input);
		
		/* GIS Parameters : Non mandatory files */
		if blocks_shapefile_file_path != nil {
			blocks_shapefile <- shape_file(blocks_shapefile_file_path,	coordinate_system_input);
		} else {blocks_shapefile <- nil;}
		
		if train_stations_shapefile_file_path != nil {
			train_stations_shapefile <- shape_file(train_stations_shapefile_file_path,	coordinate_system_input);
		} else {train_stations_shapefile <- nil;}
	}
	
	action load_modules {
		/* Mandatory modules */
		list<init_module> modules_to_load <- [];
		create streets_module				with: [shapefile::streets_shapefile]			{add item: self to: modules_to_load;}
		create routes_module				with: [shapefile::routes_shapefile]				{add item: self to: modules_to_load;}
		create houses_module				with: [shapefile::houses_shapefile]				{add item: self to: modules_to_load;}
		create hospitals_module				with: [shapefile::hospitals_shapefile]			{add item: self to: modules_to_load;}
		create frontiers_module				with: [shapefile::frontiers_shapefile] 			{add item: self to: modules_to_load;}
		create workplaces_module			with: [shapefile::workplaces_shapefile]			{add item: self to: modules_to_load;}
		create census_radios_module			with: [shapefile::census_radios_shapefile]		{add item: self to: modules_to_load;}
		
		/* Non mandatory modules */
		if blocks_shapefile != nil {
			create blocks_module			with: [shapefile::blocks_shapefile]				{add item: self to: modules_to_load;}}
		if train_stations_shapefile != nil {
			create train_stations_module	with: [shapefile::train_stations_shapefile]		{add item: self to: modules_to_load;}}

		/* Available activities */
		list<activity> all_activities <- [];
		create working_outside				with: [priority::1, is_active::(external_workers_amount > 0), workers_amount::external_workers_amount]	{add self to: all_activities;}
		create essential_service			with: [priority::2, is_active::(essential_workers_amount > 0), workers_amount::essential_workers_amount]{add self to: all_activities;}
		create generic_work 				with: [priority::3, is_active::true]																	{add self to: all_activities;}
		create kids_at_school				with: [priority::4, is_active::schools_are_open] 														{add self to: all_activities;}
		create hospitalize 					with: [priority::5, is_active::true]																	{add self to: all_activities;}
		create self_quarantine 				with: [priority::6, is_active::people_get_self_quarantine] 												{add self to: all_activities;}
		create supplying_goods 				with: [priority::7, is_active::(supplying_ratio > 0)]	 												{add self to: all_activities;}
		create kids_at_park 				with: [priority::8, is_active::true]	 																{add self to: all_activities;}
		
		/* Available restrictions */
		list<restriction> all_restrictions <- [];
		create allow_exceptions 			with: [is_active::allow_excepted]				{add self to: all_restrictions;}
		create radial_distance																{add self to: all_restrictions;}
		create scheduler_module 			
			with: [
				current_activities::(all_activities where each.is_active) sort_by (each.priority), 
				current_restrictions::all_restrictions] {add item: self to: modules_to_load;}
		
		loop each_module over: modules_to_load {ask each_module {do initialize;}}
	}
}