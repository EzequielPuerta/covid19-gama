/***
* Name: ExportCSV
* Author: Ezequiel Puerta
* Description: Exportar archivos CSV
***/

model ExportCSV

import "Constants.gaml"

global {
	map collected_csv;
	
	/* Export as CSV */
	action save_as_csv(map columns, string simulation_name, string output_file) {
		string file_path <- world.output_csv_path_for(simulation_name, output_file);
		if(not(file_exists(file_path))) {
			save list_as_delimited_string(columns.keys, ';') to: file_path type: "text" rewrite: true;}
		save list_as_delimited_string(columns.values, ';') to: file_path type: "text" rewrite: false;}
			
	/* Collect as CSV */
	action collect_csv(map columns, string simulation_name, string output_file) {
		string file_path <- world.output_csv_path_for(simulation_name, output_file);
		if !(collected_csv.keys contains file_path) {put (list_as_delimited_string(columns.keys, ';') + '\n') at: file_path in: collected_csv;}
		put (string(collected_csv at file_path) + list_as_delimited_string(columns.values, ';') + '\n') at: file_path in: collected_csv;}
		
	/* Export static species only at begining */
	action export_initial_csv {
		loop sim over: ExportCSV_model {
			ask sim.street {
				ask world {do save_as_csv(['id'::myself.name, 'geometry'::world.as_wkt_linestring(myself)], sim.name, "streets.csv");}}
			ask sim.route {
				ask world {do save_as_csv(['id'::myself.name, 'geometry'::world.as_wkt_linestring(myself)], sim.name, "routes.csv");}}
			ask sim.house {
				ask world {do save_as_csv(['id'::myself.name, 'geometry'::world.as_wkt_polygon(myself)], sim.name, "houses.csv");}}
			ask sim.building {
				ask world {do save_as_csv(['id'::myself.name, 'geometry'::world.as_wkt_polygon(myself)], sim.name, "buildings.csv");}}
			ask sim.hospital {
				ask world {do save_as_csv(['id'::myself.name, 'geometry'::world.as_wkt_polygon(myself)], sim.name, "hospitals.csv");}}
			ask sim.frontier {
				ask world {do save_as_csv(['id'::myself.name, 'geometry'::world.as_wkt_point(myself.location)], sim.name, "frontiers.csv");}}
			ask sim.census_radio {
				ask world {do save_as_csv([
					'id'::myself.name, 
					'icv'::myself.icv_value, 
					'density'::myself.density, 
					'population'::myself.population, 
					'houses'::myself.houses_amount, 
					'geometry'::world.as_wkt_polygon(myself)], sim.name, "census_radios.csv");}}
			ask sim.individual {
				ask world {do save_as_csv([
					'id'::myself.name, 
					'age'::myself.bio.age, 
					'sex'::myself.bio.sex,
					'home'::myself.home.name], sim.name, "population.csv");}}}}

	/* Dump as CSV */
	action dump_csv {
		loop to_dump over: collected_csv.pairs {
			save to_dump.value to: string(to_dump.key) type: "text" rewrite: false;
			put '' at: to_dump.key in: collected_csv;}}
	
	reflex dump_csv_on when: world.is_time_to_dump() {do dump_csv;}
	
	/* Export CSV always */
	reflex export_monitors {
		loop sim over: ExportCSV_model {
			do save_as_csv([
		        "#ElapsedTime"::world.elapsed_time(),
		        "Cycle"::cycle,
		        "Datetime"::current_date, 
		        "#Susceptible"::(individual count (each.bio.is_susceptible())),
		        "#Exposed"::(individual count (each.bio.is_exposed())),
		        "#Asymptomatic"::(individual count (each.bio.is_asymptomatic())),
		        "#Presymptomatic"::(individual count (each.bio.is_presymptomatic())),
		        "#Mild_Symptomatic"::(individual count (each.bio.is_mild_symptomatic())),
		        "#Severe_Symptomatic"::(individual count (each.bio.is_severe_symptomatic())),
		        "#ICU_Symptomatic"::(individual count (each.bio.is_icu_symptomatic())),
		        "#Recovered"::(individual count (each.bio.is_recovered())),
		        "#Dead"::(individual count (each.bio.is_dead())),
		        "#Symptomatic"::(individual count (each.bio.is_symptomatic())),
		        "#Infected"::(individual count (each.bio.is_infected())),
		        "#Infectious"::(individual count (each.bio.is_infectious())),
		        "#Resting"::(individual count each.is_resting()),
		        "#Moving"::(individual count each.is_moving()),
		        "#Working"::(individual count each.is_working()),
		        "#Studying"::(individual count each.is_studying()),
		        "#OutOfMap"::(individual count each.is_out_of_map()),
		        "#Supplying"::(individual count each.is_supplying()),
		        "#PlayingAtPark"::(individual count each.is_playing_at_park()),
		        "#Quarantine"::(individual count each.is_in_quarantine()), 
		        "#Treatment"::(individual count each.is_in_treatment()),
		        "#Death"::(individual count (each.bio.is_dead())),
		        "%DeathYoungM"::((individual count (each.bio.is_dead() and (each.bio.is_male() and each.is_young()))) * 100 / world.young_male_amount),
		        "%DeathYoungF"::((individual count (each.bio.is_dead() and (each.bio.is_female() and each.is_young()))) * 100 / world.young_female_amount),
		        "%DeathAdultM"::((individual count (each.bio.is_dead() and (each.bio.is_male() and each.is_adult()))) * 100 / world.adult_male_amount),
		        "%DeathAdultF"::((individual count (each.bio.is_dead() and (each.bio.is_female() and each.is_adult()))) * 100 / world.adult_female_amount),
		        "%DeathOlderAdultM"::((individual count (each.bio.is_dead() and (each.bio.is_male() and each.is_older_adult()))) * 100 / world.older_adult_male_amount),
		        "%DeathOlderAdultF"::((individual count (each.bio.is_dead() and (each.bio.is_female() and each.is_older_adult()))) * 100 / world.older_adult_female_amount),
		        "CommunityTransmissionCase"::individual count (each.is_local_case()),
		        "ImportedCase"::individual count (each.is_imported_case()),
		        "ExternalContactCase"::individual count (each.is_external_contact_case()),
		        "BuildingViralLoadCase"::individual count (each.is_contamination_case()),
		        "RemainingGeneralCapacity"::hospital sum_of (each.remaining_general_capacity()),
		        "RemainingICUCapacity"::hospital sum_of (each.remaining_icu_capacity()),
		        "#NoQuarantined"::individual count (each.bio.is_mild_symptomatic() and !each.is_in_quarantine()),
		        "#Quarantined"::individual count (each.bio.is_mild_symptomatic() and each.is_in_quarantine()),
		        "#NoHospitalized"::individual count ((each.bio.is_severe_symptomatic() or each.bio.is_icu_symptomatic()) and !each.bio.is_on_treatment()),
		        "#OnSevereTreatment"::individual count (each.bio.is_severe_symptomatic() and each.bio.is_on_severe_treatment()),
		        "#OnICUTreatment"::individual count (each.bio.is_icu_symptomatic() and each.bio.is_on_icu_treatment())],
	       			sim.name, "monitors.csv");}}

	reflex export_disease_evolution when: world.is_time_to_collect() {
		loop sim over: ExportCSV_model {
			loop case over: sim.world.disease_cases_evolution {
				ask world {do collect_csv([
					'current_date'::string(current_date),
					'current_cycle'::cycle,
					'new_case'::case[0],
					'disease_case'::case[1], 
					'source_case'::case[2],
					'location_case'::case[3]], sim.name, "cases_evolution.csv");}}
			disease_cases_evolution <- [];
							
			loop case over: sim.world.disease_states_evolution {
				ask world {do collect_csv([
					'current_date'::string(current_date),
					'current_cycle'::cycle,
					'person'::case[0],
					'state'::case[1],
					'infections'::case[2],
					'last_disease_state'::case[3],
					'prev_state_date'::case[4],
					'next_state_date'::case[5],
					'will_be_asymptomatic'::case[6],
					'will_die'::case[7],
					'infectivity_duration'::case[8]], sim.name, "states_evolution.csv");}}
			disease_states_evolution <- [];}}
}