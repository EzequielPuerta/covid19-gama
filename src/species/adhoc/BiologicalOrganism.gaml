/***
* Name: BiologicalOrganism
* Author: Ezequiel Puerta
* Description: Organismo Biológico
***/

model BiologicalOrganism

import "../../core/Constants.gaml"
import "DemographicRange.gaml"

species biological_organism control: fsm {
	/* Generic Attributes */
	individual person;
	demographic_range range;
	int age;
	string sex;
	int state_code;
	bool configured_by_external_source <- false;
	
	bool is_male {return self.sex = male;}
	bool is_female {return self.sex = female;}
	bool is_young {return self.range.is_young();}
	bool is_adult {return self.range.is_adult();}
	bool is_older_adult {return self.range.is_older_adult();}
	
	/* Disease Attributes */
	date infection_date;
	date prev_state_date;
	date next_state_date;
	int treatment_status;
	string last_disease_state;
	float infectivity_duration;
	bool will_die;
	bool will_be_asymptomatic;
	init {do out_of_treatment();}
	
	/* Testing */
	bool is_susceptible {return self.state = susceptible;}
	bool is_exposed {return self.state = exposed;}
	bool is_asymptomatic {return self.state = asymptomatic;}
	bool is_presymptomatic {return self.state = presymptomatic;}
	bool is_mild_symptomatic {return self.state = mild_symptomatic;}
	bool is_severe_symptomatic {return self.state = severe_symptomatic;}
	bool is_icu_symptomatic {return self.state = icu_symptomatic;}
	bool is_recovered {return self.state = recovered;}
	bool is_dead {return self.state = dead;}	
	bool is_symptomatic {return [mild_symptomatic, severe_symptomatic, icu_symptomatic] contains self.state;}
	bool is_infectious {return self.is_asymptomatic() or self.is_presymptomatic() or self.is_symptomatic();}
	bool is_infected {return self.is_exposed() or self.is_infectious();}
	
	/* Treatment */
	action out_of_treatment {self.treatment_status <- no_treatment;}
	action on_severe_treatment {self.treatment_status <- severe_treatment;}
	action on_icu_treatment {self.treatment_status <- icu_treatment;}
	bool is_out_of_treatment {return !self.is_dead() and self.treatment_status = no_treatment;}
	bool is_on_severe_treatment {return !self.is_dead() and self.treatment_status = severe_treatment;}
	bool is_on_icu_treatment {return !self.is_dead() and self.treatment_status = icu_treatment;}
	bool is_on_treatment {return self.is_on_severe_treatment() or self.is_on_icu_treatment();}
	
	/* Auxs */
	date advance_to(float a_duration) {
		date possible_next_state_date <- prev_state_date + a_duration;
		date result;
		if current_date > possible_next_state_date {result <- current_date;} 
		else {result <- possible_next_state_date;}
		return result;}
	
	date calculate_with(float mean, float st_deviation) {
		date possible_next_state_date <- prev_state_date + world.normal_from(mean, st_deviation);
		date result;
		if current_date > possible_next_state_date {result <- current_date;} 
		else {result <- possible_next_state_date;}
		return result;}
	
	bool reached_date {return current_date >= self.next_state_date;}
	
	float fatality_rate {
		switch self.sex {
		   	match male {return world.normal_from(self.range.man_fatality_rate, self.range.st_dev_for_man_fatality_rate);}
		   	match female {return world.normal_from(self.range.woman_fatality_rate, self.range.st_dev_for_woman_fatality_rate);}}}
	
	action log_state_transition {
		world.disease_states_evolution << [
			self.person.name, 
			state, 
			length(self.person.infections), 
			self.last_disease_state,
			self.prev_state_date,
			self.next_state_date,
			self.will_be_asymptomatic,
			self.will_die,
			self.infectivity_duration];}

	/* FSM Epidemiological States */
	/*
	 * ============================================================================================
	 * |                     |              |                               |                     |
	 * |     Susceptible     |    Exposed   |          Infectious           |  Recovered or Dead  |
	 * |                     |              |                               |                     |
	 * ========================================================================== >>> TIME >>> ====
	 *                       |----latent----|
	 *                       |------incubation------|------symptomatic------|
	 *                                              |-------recovery--------|
	 * ____________________________________________________________________________________________
	 * 
	 *       Su ----------> Ex -----------> Ps ---> MS ---> SS ---> IS -----------> De
	 *                       |                      |        |       |   
	 *                       |                      |        |       |
	 *                       |------------> As ------>-------->------->----> Re
	 * 
	 * ========================================================================== >>> TIME >>> ====
	 * Susceptible:			Su
	 * Exposed:				Ex
	 * Asymptomatic:		As
	 * Presymptomatic:		Ps
	 * Mild Symptomatic:	MS
	 * Severe Symptomatic:	SS
	 * ICU Symptomatic:		IS
	 * Recovered:			Re
	 * Dead:				De  
	 */
	
	state susceptible initial: true {
		enter {
			if self.configured_by_external_source {
				self.configured_by_external_source <- false;
			} else {
				self.state_code <- 0;}
			do log_state_transition();}
				
		transition to: exposed when: self.infection_date != nil;
		
		exit {self.prev_state_date <- current_date;}
	}
	
	state exposed {
		enter {
			if self.configured_by_external_source {
				self.configured_by_external_source <- false;
			} else {
				self.state_code <- 1;
				self.last_disease_state <- exposed;
				self.next_state_date <- self.calculate_with(mean_latent_days, st_deviation_latent_days);
				self.will_be_asymptomatic <- flip(asymptomatic_ratio);}
			do log_state_transition();}
				
		transition to: asymptomatic when: self.reached_date() and self.will_be_asymptomatic;
		transition to: presymptomatic when: self.reached_date() and !self.will_be_asymptomatic;
		
		exit {self.prev_state_date <- current_date;}
	}
	
	state asymptomatic {
		enter {
			if self.configured_by_external_source {
				self.configured_by_external_source <- false;
			} else {
				self.state_code <- 2;
				self.last_disease_state <- asymptomatic;
				float mean <- max(7, (mean_mild_recovery_days + mean_icu_recovery_days)/2 + (mean_incubation_days - mean_latent_days));
				float st_deviation <- max(1, (st_deviation_mild_recovery_days + st_deviation_icu_recovery_days)/2 + (st_deviation_incubation_days - st_deviation_latent_days));
				self.next_state_date <- self.calculate_with(mean, st_deviation);}
			do log_state_transition();}
				
		transition to: recovered when: self.reached_date();
		
		exit {self.prev_state_date <- current_date;}
	}
	
	state presymptomatic {
		enter {
			if self.configured_by_external_source {
				self.configured_by_external_source <- false;
			} else {
				self.state_code <- 3;
				
				float mean <- max(2, mean_incubation_days - mean_latent_days);
				float st_deviation <- max(0.5, st_deviation_incubation_days - st_deviation_latent_days);
				self.next_state_date <- self.calculate_with(mean, st_deviation);
				
				if flip(self.fatality_rate()) {
					self.will_die <- true;
					self.last_disease_state <- icu_symptomatic;
				} else {
					self.will_die <- false;
					map<string, float> probabilities <- [];
					probabilities << mild_symptomatic::mild_ratio;
					probabilities << severe_symptomatic::severe_ratio;
					probabilities << icu_symptomatic::icu_ratio;
					self.last_disease_state <- world.multinomial_from(probabilities);}
			
				switch self.last_disease_state {
					match mild_symptomatic {self.infectivity_duration <- world.normal_from(mean_mild_recovery_days, st_deviation_mild_recovery_days);}
					match severe_symptomatic {self.infectivity_duration <- world.normal_from(mean_severe_recovery_days, st_deviation_severe_recovery_days);}
					match icu_symptomatic {self.infectivity_duration <- world.normal_from(mean_icu_recovery_days, st_deviation_icu_recovery_days);}}}
			do log_state_transition();}
					
		transition to: mild_symptomatic when: self.reached_date();
		
		exit {self.prev_state_date <- current_date;}
	}
	
	state mild_symptomatic {
		enter {
			if self.configured_by_external_source {
				self.configured_by_external_source <- false;
			} else {
				self.state_code <- 4;
				if self.last_disease_state = self.state {
					self.next_state_date <- self.advance_to(self.infectivity_duration);
					self.infectivity_duration <- 0.0;
				} else {
					float mean <- 7 #days;
					float st_deviation <- 1 #day;
					float current_state_duration <- world.normal_from(mean, st_deviation);
					self.next_state_date <- self.advance_to(current_state_duration);
					self.infectivity_duration <- self.infectivity_duration - current_state_duration;}}
			do log_state_transition();}
					
		transition to: severe_symptomatic when: self.reached_date() and (self.last_disease_state != self.state);
		transition to: recovered when: self.reached_date() and (self.last_disease_state = self.state);
		
		exit {self.prev_state_date <- current_date;}
	}
	
	state severe_symptomatic {
		enter {
			if self.configured_by_external_source {
				self.configured_by_external_source <- false;
			} else {
				self.state_code <- 5;
				if self.last_disease_state = self.state {
					self.next_state_date <- self.advance_to(self.infectivity_duration);
					self.infectivity_duration <- 0.0;
				} else {
					float mean <- 7 #days;
					float st_deviation <- 1 #day;
					float current_state_duration <- world.normal_from(mean, st_deviation);
					self.next_state_date <- self.advance_to(current_state_duration);
					self.infectivity_duration <- self.infectivity_duration - current_state_duration;}}
			do log_state_transition();}
					
		transition to: icu_symptomatic when: self.reached_date() and (self.last_disease_state != self.state);
		transition to: recovered when: self.reached_date() and (self.last_disease_state = self.state);
		
		exit {self.prev_state_date <- current_date;}
	}
	
	state icu_symptomatic {
		enter {
			if self.configured_by_external_source {
				self.configured_by_external_source <- false;
			} else {
				self.state_code <- 6;
				self.next_state_date <- self.advance_to(self.infectivity_duration);
				self.infectivity_duration <- 0.0;}
			do log_state_transition();}
				
		transition to: recovered when: self.reached_date() and !self.will_die;
		transition to: dead when: self.reached_date() and self.will_die;
		
		exit {self.prev_state_date <- current_date;}
	}
	
	state recovered final: true {
		enter{
			if self.configured_by_external_source {
				self.configured_by_external_source <- false;
			} else {
				self.state_code <- 7;
				self.next_state_date <- current_date;}
			do log_state_transition();}}
	
	state dead {
		enter{
			if self.configured_by_external_source {
				self.configured_by_external_source <- false;
			} else {
				self.state_code <- 8;
				self.next_state_date <- current_date;}
			do log_state_transition();}}
}