/***
* Name: Frontier
* Author: Ezequiel Puerta
* Description: Acceso de Frontera
***/

model Frontier

import "AbstractTarget.gaml"
import "../vehicles/Vehicle.gaml"
import "../geography/Route.gaml"

species frontier parent: abstract_target {
	bool is_for_essential_activity <- true;
	int inflow_per_hour;
	int outflow_per_hour;
	float disease_rate;
	float current_rate;
	frontier destination_frontier;
	
	/* Configuration */
	reflex calculate_disease_rate when: world.is_new_day() {
		self.current_rate <- (individual count (each.bio.is_infectious())) / world.individual_amount * self.disease_rate;}
	
	action set_destination_frontier(frontier destination) {destination_frontier <- destination;}
	
	reflex generate_new_vehicle {
		int inflow_amount <- poisson(step * inflow_per_hour / 1 #h);	
		create vehicle number: inflow_amount with: [
			location::self.location,
			origin::self.location, 
			target::self.destination_frontier.location,
			speed::40 #km/#h + (gauss(0.0, 5.0)) #km/#h] {
				if flip(myself.current_rate) {
					self.driver_exposed <- true;
				} else {
					self.driver_exposed <- false;
				}
		}
	}
	
	/* Visiting */
	action arrives(individual person) {
		ask person {
			if self.bio.is_susceptible() and flip(myself.current_rate) {
				do get_infected_as(imported_case);
				world.disease_cases_evolution << [self.name, imported_case, myself.name, world.as_wkt_point(self.location)];}}}
	
	action leaves(individual person) {}
	
	/* Aspect */
	aspect geom {
		draw square(100 #m) color: #blue border: #black at: location;
	}
}