/***
* Name: Hospital
* Author: Ezequiel Puerta
* Description: Hospital
***/

model Hospital

import "ConcreteTarget.gaml"

species hospital parent: concrete_target {
	bool is_for_essential_activity <- true;
	int general_capacity;
	int icu_capacity;
	list<individual> general_in_use <- [];
	list<individual> icu_in_use <- [];
	
	bool is_full {return self.general_is_full() and self.icu_is_full();}
	bool general_is_full {return length(general_in_use) = general_capacity;}
	bool icu_is_full {return length(icu_in_use) = icu_capacity;}
	int remaining_general_capacity {return general_capacity - length(general_in_use);}
	int remaining_icu_capacity {return icu_capacity - length(icu_in_use);}
	
	reflex update_current_capacity {
		list<individual> get_worse <- general_in_use where each.bio.is_icu_symptomatic();
		loop severe_patient over: get_worse {
			if !self.icu_is_full() {
				remove severe_patient from: general_in_use;
				add severe_patient to: icu_in_use;	
			} else {
				break;
			}
		}
	}
	
	action arrives(individual person) {
		invoke arrives(person);
		if person.doing.task.for_treatment() {
			if person.bio.is_icu_symptomatic() and !self.icu_is_full() {
				add person to: icu_in_use;
				ask person.bio {do on_icu_treatment();}
			} else if person.bio.is_severe_symptomatic() and !self.general_is_full() {
				add person to: general_in_use;
				ask person.bio {do on_severe_treatment();}
			} else if person.bio.is_severe_symptomatic() and !self.icu_is_full() {
				add person to: icu_in_use;
				ask person.bio {do on_severe_treatment();}
			} else {
				ask person.bio {do out_of_treatment();}
			}
		}
	}
	
	action leaves(individual person) {
		invoke leaves(person);
		ask person.bio {if self.is_on_treatment() {do out_of_treatment();}}
		ask person {
			if myself.icu_in_use contains self {
				remove self from: myself.icu_in_use;
			} else if myself.general_in_use contains self {
				remove self from: myself.general_in_use;
			}
		}
	}
	
    aspect geom {
    	draw "+" color: #red border: #black font: font('Default', 25, #bold) at: location + {-60 #m, 60 #m, 6};
    }
}