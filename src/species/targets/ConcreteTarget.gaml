/***
* Name: ConcreteTarget
* Author: Ezequiel Puerta
* Description: Cualidades genericas de un edificio
***/

model ConcreteTarget

import "AbstractTarget.gaml"
import "../../core/Functions.gaml"
import "../agents/Individual.gaml"

species concrete_target parent: abstract_target {
	map<individual, bool> recurring_visitors_status;
	int recurring_visitors_amount {return length(recurring_visitors_status);}
	
	/* Auxs */
	action visitor_arrived(individual person) {
		put true in: recurring_visitors_status at: person;}
	action visitor_leaves(individual person) {
		put false in: recurring_visitors_status at: person;}
	bool is_present(individual person) {
		return recurring_visitors_status at person;}
	list<individual> current_visitors {
		return recurring_visitors_status.keys where self.is_present(each);}
	
	/* Testing */
	bool is_concrete_target {return true;}
	
	list<date> viral_load <- [];
	float current_viral_load_percentage <- 0.0;
	
	/* Working */
	action register_as_recurrent(individual person) {
		do visitor_leaves(person);}
		
	reflex visitors_moving_inside {
		loop visitor over: self.current_visitors() {
			ask visitor {
				do goto target: any_location_in(myself) on: world.street_network;
				do infect;}}}
	
	/* Visiting */
	action arrives(individual person) {
		do visitor_arrived(person);
		if person.bio.is_infectious() {
			do increase_viral_load();
		} else if person.bio.is_susceptible() {
			do infect_to(person);
		}
	}
	
	action leaves(individual person) {do visitor_leaves(person);}
	
	/* Viral Contamination */
	action increase_viral_load {
		date right_now <- world.current_date;
		if right_now != nil {add item: right_now to: self.viral_load;}}
	
	action infect_to(individual visitor) {
		if flip(self.current_viral_load_percentage) {
			ask visitor {
				do get_infected_as(contamination_case);
				world.disease_cases_evolution << [self.name, contamination_case, myself.name, world.as_wkt_point(self.location)];}}}
	
	reflex update_viral_load when: length(viral_load) > 0 {
		int active_viral_loads <- 0;
		int total_viral_loads <- length(viral_load)-1;
		
		loop index from: total_viral_loads to: 0 {
			if world.days_since(viral_load[index]) >= days_on_surfaces {break;} 
			else {active_viral_loads <- active_viral_loads+1;}}
		current_viral_load_percentage <- min(1.0, (active_viral_loads * expected_contaminated_surface) / shape.area);
	}
	
	/* Aspect */
	aspect viral {
		list<rgb> colors <- [#dimgray, #darkkhaki, #tan, #khaki, #yellow, #gold, #orange, #orangered, #red, #darkred];
    	rgb current_color <- world.current_color_of(current_viral_load_percentage, 1.0, colors);
		draw shape color: current_color empty: false at: location + {0,0,4};
	}
}