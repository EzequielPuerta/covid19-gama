/***
* Name: Block
* Author: Ezequiel Puerta
* Description: Manzanas
***/

model Block

import "../targets/House.gaml"

species block {
	string id;
	string id_census_radio;
	int houses_amount;
	float icv_value;
	int population;
	
	list<house> houses;
	
	float value {return icv_value;}
    aspect geom {
    	rgb current_color <- #black;
    	switch icv_value {
    		match_between [0.0,1.0] {current_color <- #maroon;} 
    		match_between [1.0,2.0] {current_color <- #darkred;}
    		match_between [2.0,3.0] {current_color <- #firebrick;}
    		match_between [3.0,4.0] {current_color <- #red;}
    		match_between [4.0,5.0] {current_color <- #orangered;}
    		match_between [5.0,6.0] {current_color <- #orange;} 
    		match_between [6.0,7.0] {current_color <- #yellow;}
    		match_between [7.0,8.0] {current_color <- #greenyellow;}
    		match_between [8.0,9.0] {current_color <- #lime;}
    		match_between [9.0,10.0] {current_color <- #limegreen;}
    	}
    	
    	draw shape color: current_color at: location + {0,0,2};
    }
}